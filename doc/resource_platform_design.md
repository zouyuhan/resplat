# 资源平台项目设计文档

| 版本 | 说明 | 时间 | 负责人 |
| :--: | :--- | :--: | :--: |
| 1.0 | 初始版本 | 2014.02.10 | 雨晗 |

# 1. 项目背景

目前，摩羯已完成Coco用户系统和捕鱼达人游戏数据统计分析系统这两套数据分析系统，他们之间相互独立，但功能上却大体相似。随着触控更多游戏统计分析迁移到摩羯，我们需要一套完整的数据仓库平台，允许用户（触控的产品人员、开发人员）更容易使用我们的系统，自己导入数据，清洗数据，编写计算程序，查询数据。这对摩羯的数据分析平台提出了更多更艰巨的挑战，也对很多模块提出了升级需求，本文意在归纳这些通用需求，并规划一个通用的解决方案，满足未来触控用户，和对外开放的需求。

这些需求包括数据管理、数据传输、计算程序开发、计算程序执行、报表展现和数据预警等多个技术领域，这些领域最终又指向一个不可分割的整体——数据仓库。这个数据仓库的建立，在触控和我们明年重点项目——UDW之间打通了一道桥梁，它能管理、收集触控所有游戏、广告方面的数据，并格式化规范化，利于在UDW中我们自己管理数据。

# 2. 名词解释

* **资源** - 资源是用户提供的有价值的信息，分为两大类：数据资源和程序资源。一份资源表示资源管理和访问的最小单位，例如一个日志在5分钟内的数据片段。所有资源都用统一的资源标识符(Uniform Resource Identifier, URI)唯一标识。

* **数据资源** - 用户上传、平台定时抓取或通过ETL程序计算生成的数据称为数据资源。目前，一份数据资源就对应一份物理数据，未来，一份数据资源可以是多份物理数据经过主键联合的逻辑资源，例如描述用户支付行为的SDK日志、服务端日志和运营商反馈日志的联合。
	* 数据资源有层次概念，例如一份资源，URI为`data://hdfs/mjyun/pb/buyudaren`，表示捕鱼达人2的所有数据，二级资源`data://hdfs/mjyun/pb/buyudaren/201301`，表示其在2013年1月的数据

* **程序资源** - 用户在平台上编写的代码称为程序资源，包括HQL、ETL脚本、运行配置、代码文档等。

* **任务资源** - 程序资源与数据资源及时间关联后，可一次性或例行执行的代码称为任务资源

# 3. 可行性分析

在项目调研阶段，我们对触控各产品线的需求进行了简单的整理，我们把需求划分为几大类：

| 需求方 | 需求方说明 | 主要需求点 | 后续需求负责人 |
| :--: | :--- | :--- | :--: |
| 摩羯 | 整合架构，整合需求 | 1. 各系统模块优化升级 <br> 2. 数据管理平台 <br> 3. 可视化运维、监控 <br> 4. 权限系统 | 雨晗 |
| Coco用户系统 | 用户系统的统计、分析需求 | 1. 维护现有Coco统计平台 <br> 2. 更加灵活的数据导入、使用方式，满足个性化需求 | 泽敏 | 
| BI游戏分析 | 触控所有游戏的统计、分析需求 | 1. 游戏数据的常规统计、运营分析 <br> 2. 用户行为分析 | 泽敏 |
| 流量中心 | 广告统计，用户特征抽取 | 1. 广告系统关键数据实时统计 <br> 2. 广告数据反馈系统 <br> 3. 用户行为分析、特征提取 <br> 4. 大量灵活的调研需求和特征计算 | 东东 |

其中，后续需求负责人是指负责整理、评估对应需求方提出的需求的人。整理后，需更新本文。

接下来我们再向曦说明几个需求方的需求点，并指明优先级。根据优先级，安排项目排期。

## 3.1 各需求方需求说明

### 3.1.1 摩羯

此处详细说明了资源平台提供的功能点，后续各个外部需求方的需求，都会被资源平台的功能点覆盖。具体功能点的优先级，可以参考后文中外部需求方的需求的优先级。

| 需求点 | 需求说明 | 相关功能点 | 优先级 |
| :--- | :--- | :--- | :--- |
| 各系统模块优化升级 | 1. 在现有Coco用户系统、捕鱼达人历史数据、捕鱼达人土豪金版本的基础上，进一步将ETL程序的各个子模块解耦、拆分、优化为多各功能独立的子模块，以便达到以下目标: <br> &nbsp;&nbsp; * 流式计算和批量计算需求分离，实时需求和离线需求分离 <br> &nbsp;&nbsp; * ETL程序分布式化 <br> &nbsp;&nbsp; * ETL程序可回溯，错误数据可重跑 <br> 2. 在现有服务的基础上，优化MJStats和Hive的表结构，满足各种查询需求。 | 1. 现有ETL程序升级及测试 <br> 2. ETL程序公共库 <br> 3. ETL设计说明文档及数据表使用文档 | **高**，长期逐步进行 |
| 数据管理平台 | 1. 为数据产出者、数据处理者、数据使用者搭建友好的Web平台，供其导入、处理、使用数据: <br> &nbsp;&nbsp; * 对满足特定Schema的数据(protobuf描述)，产出标准的统计结果（类似现有Coco统计平台)，由标准的Web界面展示 <br> &nbsp;&nbsp; * 对个性化数据，由用户编写ETL程序(python)，提供Web界面辅助用户编写ETL程序，并提供Web测试环境 <br> &nbsp;&nbsp; * 对个性化数据，使用Hive进行查询，提供Web界面辅助HQL编写，并通过图表展示HQL查询结果 | 1. 数据传输平台 <br> 2. 元数据管理系统(管理数据资源、程序资源及其配置)，及Web前端 <br> 3. 计算任务管理(MJFlows)和任务调度系统(可复用现有Hive Scheduler)，及Web前端 <br> 4. 数据展现系统（MJStats选表，Hive HQL编写工具，Web展现平台 | **重要** |
| 可视化运维、监控 | 1. 对数据管理平台中，数据导入、数据处理任务的数据传输、数据质量、数据处理情况进行监控 <br> 2. 对集群中机器状态进行监控 | 1. 数据管理平台中操作的状态、日志记录 <br> 2. 复用、优化现有基于Tsar的监控系统 | 中 |
| 权限系统 | 1. 对数据资源和程序资源，为不同的角色设定不同的读写权限 <br> 2. 线上线下资源分离<br> &nbsp;&nbsp; * 资源按线上、线下维度完全隔离（线上资源通常指重要的例行报表、例行分析，线下资源是指产品、开发人员根据调研需求产出的程序资源和数据资源）。<br> &nbsp;&nbsp; * 线上数据相关操作必须由专人在指定流程下操作。 <br> &nbsp;&nbsp; 线上数据资源能作为线下数据资源的输入，但线下数据资源不能作为线上数据资源的输入 <br> &nbsp;&nbsp; * 建立线上程序资源的测试、上线、回滚机制 <br> &nbsp;nbsp; * 建立线下程序资源简易、无审批操作流程，方便调研计算 | 1. 元数据管理系统(权限管理)，及Web前端 <br> 2. ETL程序(python脚本)的测试，并线运行机制 | 低 |

### 3.1.2 Coco用户系统

| 需求点 | 需求说明 | 相关功能点 | 优先级 |
| :--- | :--- | :--- | :--- |
| 维护现有Coco统计平台 | 1. 在完成ETL程序升级后，将原有Coco统计平台的ETL程序迁移到新的资源平台 <br> 2. 将Coco统计平台的数据查询、展现迁移到资源平台的Web端 | 1. ETL程序升级、测试 <br> 2. 展现端Web开发 | 低 |
| 更加灵活的数据导入、使用方式，满足个性化需求 | 1. 在资源平台提供数据导入管理，允许Coco用户系统的开发人员将新数据导入到资源平台 <br> 2. 提供特定Schema，对按该Schema描述的数据，产出标准的统计报表 | | **高** |
| | 3. 对个性化数据（protobuf描述)，允许开发人员编写ETL脚本(python)处理数据，输出新的数据资源 <br> 4. 对所有数据资源，允许开发人员编写HQL通过Hive查询数据，并拉取计算结果 | |中 |
| | 5. 开发数据展现系统，通过图表展现标准报表及Hive查询结果 | |低 |

### 3.1.3 BI游戏分析

| 需求点 | 需求说明 | 相关功能点 | 优先级 |
| :--- | :--- | :--- | :--- |
| 游戏数据的常规统计、运营分析 | 1. 提供特定Schema，对按该Schema描述的数据，产出标准的统计报表（同Coco用户系统) | | **高** |
| 用户行为分析 | 1. 根据玩家在游戏中的行为数据，产出特定的分析结果 | 1. Event大表、User大表的数据建设 2. 例行挖掘算法的调研及开发 | 低 |

### 3.1.4 流量中心

| 需求点 | 需求说明 | 相关功能点 | 优先级 |
| :--- | :--- | :--- | :--- |
| 广告系统关键数据实时统计 | 1. 通过实时流式计算，满足广告系统分钟级的统计需求，展示广告投放、转化、跟踪统计数据。<br> 2. 引入反作弊策略，用于修正统计结果 | 1. 广告系统统计ETL程序 <br> 2. 初步凡作弊策略调研及程序开发  <br> 3. 报表Web展现系统  | 中 |
| 广告数据反馈系统 | 1. 通过广告的投放、转化数据，反馈广告投放效果用于广告投放策略 | 1. 广告投放策略调研及程序开发 | 中 |
| 用户行为分析、特征提取 | 1. 结合游戏数据及App信息等，分析用户兴趣点，并影响广告投放策略 <br> 2. 通过用户点击数据，分析用户对广告类型的兴趣，影响广告投放策略 <br> 3. 通过机器挖掘算法提取用户特征(不可读) | 1. 策略、算法调研及程序开发 | 低 |
| 大量灵活的调研需求和特征计算 | 1. 允许流量中心的产品、开发人员通过资源平台编写ETL程序、挖掘算法，使用数据 |  | 低 |

## 3.2 预期目标

完成项目所有高、中、低优先级的需求后，我们应该得到这样的一个资源平台：

* 总体架构 
	* 提供一个完整的数据仓库，将数据、程序、任务抽象为资源，以资源为单位提供管理、访问接口及权限控制
	* 用户可通过Web平台自助使用
* 数据管理
	* 数据统一的导入、处理、使用方式
	* 提供元数据管理页面，数据的实效性、处理进度、数据质量、数据量一目了然
	* 资源分线上线下，确保线上资源稳定、安全、可靠，确保线下资源易用
	* 数据总量及更新量巨大，保存在HDFS中，数据只增不删改，保证数据持久性
	* 严格的权限控制
* 计算管理
	* 计算任务可配置(例行执行时间、依赖资源)，计算任务多数是周期性的，但也存在用于调研、试验目的的临时计算
	* 数据在资源平台里进行各种计算，以生成新的数据，数据依赖关系可能很复杂，但不存在环（DAG计算模型），资源平台对数据流有很好的管理
	* 数据一般只增不改，且一份数据只有一个确定的生成程序，无需复杂的并发控制和事务控制。
	* 线上计算任务可测试，可回滚，确保线上数据稳定可靠
* 数据传输、处理
	* 数据格式化(protobuf)
	* 支持用户自定义ETL过程(Python脚本)，使生产环境数据更容易导入数据仓库，并能产出自己的数据
	* 完整的Web开发环境，提高用户开发ETL程序的易用性、正确性

在未来，资源平台还会在数据计算的灵活性、性能及数据质量监控上做更多的事情，例如：

* 数据处理
	* 提供方便的异构存储结构化数据读写功能支持和数据表连接功能支持
	* 允许计算任务依赖多个大小不一的数据源
	* 支持分布式计算模型，并且在map/reduce模型下支持连续进行多轮map/reduce
	* 支持一次计算会生成多个数据输出
	* ……
* 数据使用
	* 支持更多的后验监控规则
	* 提供标准化API获取数据，支持与其它平台集成
	* ……

# 4. 用例描述

此处，针对资源平台提供的各种功能，依次描述其使用情景。功能分为四大类：数据管理，如数据导入，数据监控情况等；计算管理，如计算任务的配置及监控；数据处理，如自定义ETL程序的编写及配置；数据使用，如Web平台提供的报表展示界面。

## 4.1 数据管理

### 4.1.1 新建数据源

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 新建数据源资源，填写元信息 | 如机器列表、权限信息、路径信息，Schema (proto文件)等 |
| 2 | 用户RD | 对于非格式化(protobuf)数据，开发相应的格式化程序 | |
| 3 | 用户RD | 上传样例数据(文本或protobuf)，运行格式化程序进行测试 | |
| 4 | 资源平台 | 根据Schema展示程序输出结果 | |
| 5 | 用户RD | 确认上述信息，提交审批 | |
| 6 | 用户RD负责人 | 审批信息，确认数据资源 | |
| 7 | 资源平台运营者 | 审批信息，确认数据资源 | |
| 8 | 资源平台 | 定时从生产环境模块转储数据，抓取数据，进行格式化计算，保存在数据管理系统中 | |

### 4.1.2 数据源管理

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD/PM | 登录数据源管理系统，可通过各种组合条件查找数据源 | 如按产品线，按字段信息查找数据 |
| 2 | 用户RD/PM | 登录数据源管理系统，打开数据源资源，查看其元信息 | |
| 3 | 用户RD/PM | 登录数据源管理系统，打开数据源资源，查看数据导入记录，即数据可用时间 | |
| 4 | 用户RD/PM | 登录数据源管理系统，打开数据源资源，对数据元信息进行删改操作 | |

### 4.1.3 数据管理

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD/PM | 登录数据源管理系统，打开数据源资源，选择关联日期，打开数据资源，查看数据样本 | |

## 4.2 程序管理

### 4.2.1 开发计算程序

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 新建程序资源，填写元信息 | |
| 2 | 资源平台 | 根据数据管理系统的元数据，提示用户可用于计算的数据资源 | |
| 3 | 用户RD | 根据提示，选择数据，选择测试数据的日期，并选择程序模式 | 如HQL，Python脚本 |
| 4 | 用户RD | 编写计算程序 | 资源平台提供公共函数库，可从函数库中选择 | |
| 5 | 资源平台 | 运行测试用力，输出程序结果 | |
| 6 | 用户RD | 提交程序资源 | |

### 4.2.2 程序管理

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 登录程序管理系统，可通过各种组合条件查找程序 | 如按产品线查找 |
| 2 | 用户RD | 登录程序管理系统，打开程序资源，查看其元信息 |

## 4.3 任务管理

### 4.3.1 启动例行任务

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 登录程序管理系统，可通过各种组合条件查找程序 | 如按产品线查找 |
| 2 | 用户RD | 登录程序管理系统，打开程序资源，查看该程序相关例行任务列表 | |
| 3 | 用户RD | 新建例行任务，从数据管理系统根据关联数据资源的时间范围，选择任务开始时间，填写任务关联时间范围，及延迟时间 | 例如某个任务，关联从2014-01-01开始的数据，延迟时间为24小时，时间范围为24小时，则其从2014-01-02 00:00:00开始执行，输入数据的时间从2014-01-01 00:00:00 ~ 2014-01-01 23:59:59 |
| 4 | 用户RD | 新建任务输出的数据资源，填写相关元信息 | |
| 5 | 用户RD | 确认上述信息，提交审批 | |
| 6 | 用户RD负责人 | 审批信息，确认数据资源 | |
| 7 | 资源平台运营者 | 审批信息，确认数据资源 | |
| 8 | 资源平台 | 定时执行任务，并保存执行记录 | |

### 4.3.2 启动临时任务

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 登录程序管理系统，可通过各种组合条件查找程序 | 如按产品线查找 |
| 2 | 用户RD | 登录程序管理系统，打开程序资源，查看该程序相关例行任务列表 | |
| 3 | 用户RD | 新建临时任务，从数据管理系统根据关联数据资源的时间范围，填写任务关联时间范围 | |
| 4 | 用户RD | 新建任务输出的数据资源，填写相关元信息 | |
| 5 | 用户RD | 确认上述信息，执行任务 | |
| 6 | 资源平台 | 执行任务，并保存执行记录 | |

### 4.3.3 任务管理

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 登录任务管理系统，可通过各种组合条件查找程序 | 如按产品线查找，按程序资源查找，按数据资源查找等 |
| 2 | 用户RD | 登录任务管理系统，打开任务资源，查看其执行记录 | |
| 3 | 用户RD | 登录任务管理系统，打开任务资源，查看其执行后输出的数据资源 | |
| 4 | 用户RD | 登录任务管理系统，打开任务资源，启动、停止、配置该任务 | |

## 4.4 数据使用

### 4.4.1 添加Web报表

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD/PM | 登录报表管理系统，新增报表或图表 | |
| 2 | 用户RD/PM | 选择报表或图表的类型，展现形式，发送频率等 | |
| 3 | 用户RD/PM | 选择报表依赖的数据资源及字段 | |
| 4 | 用户RD/PM | 查看生成后的报表和图表 | |

### 4.4.2 使用Hive查询数据

与启动临时任务类似，任务类型为HQL。

# 5. 设计思路及折衷

## 5.1 资源平台的基本接口

资源平台的程序模块是独立运行的，提供接口为用户的程序或Web界面访问，整理如下：

* 元数据相关的接口:
	1. 获取、修改元数据
* 资源相关的接口:
	1. 一份HDFS资源，或者指定的一个或多个Partition作为MapReduce/Hive/报表任务的输入
	2. 一份单机资源，通过rsync/wget/ftp下载，作为MapReduce/报表任务的输入
	3. 一份MJStats资源，作为MapReduce/报表任务的输入
	4. 一份HDFS资源，或者指定的一个或多个Partition作为MapReduce/Hive任务的输出
	5. 一份MJStats资源，作为特定MapReduce/Hive任务的输出
	6. 一份单机资源，通过rsync/wget/ftp下载到本地
	7. 一份HDFS资源，或者指定的一个或多个Partition，通过Hadoop client下载到本地
	8. 一份MJStats资源，通过MySQL直接查询或导出

上述接口，都是基于MySQL或者其他开源计算平台提供的基本的数据查询服务，已经能满足大部分用户对资源的读写需求。但这些接口需要暴露底层存储模型，也不便于用户访问存储在多个模块中的数据，一期可以先这么实现。

## 5.2 资源平台模块设计

* 元数据管理模块 **MetaService**
	* 所有数据存储在MySQL中，部分操作需要依赖事务
	* 提供数据资源、程序资源、任务资源等相关元信息的增、删、改、查接口
* 数据源及数据资源管理模块 **DataMgr**
	* 数据可以存储在单机文件系统、HDFS和MJStat中，相关URI由 **MetaService** 存储
	* 根据URI获取数据，需要针对不同的存储系统实现不同的查询程序，例如数据存储在HDFS时，则将URI转换成HDFS路径；数据存储在单机文件系统中时，则将URI转换成wget路径；数据存储在MJStats中时，则将URI转换成SQL及相关MySQL地址
	* 将数据存储在特定URI中，需要针对不同的文件系统实现不同的导入程序，其中MJStats的导入较为复杂，一起可以实现为只允许导入特定Schema的数据。
	* 管理各个数据源的定时导入
* 任务资源管理模块 **TasksMgr**
	* 提供基于时间的定时调度和基于依赖关系的调度
* 程序资源管理模块 **ScriptsMgr**
	* 平台需要提供三种计算模式：
		* 基于MJFlows的流式计算
		* 基于MapReduce的批量计算
		* 基于Hive的HQL查询
		* 一期，我们只提供基于MJFlows的流式计算模式和基于Hive的HQL查询模式。
	* HQL
		* 调度Hive程序，程序资源为一段HQL查询语句，任务管理模块负责提交Hive任务，监控Hive任务执行结果，将任务输出通过URI写入数据资源模块，并在任务出错时重提Hive任务
	* MJFlows Worker
		* 提供一个Python编程框架，由用户实现一个函数，其输入输出分别为一行日志。框架的输入输出负责从MJFlows获取任务，处理其依赖的数据资源的输入输出。
		* 提供Python的函数库，方便用户开发Python脚本，例如日期相关的转换库，IP地址库，字符字典库等
		* 程序输入输出只能为存储在HDFS上的数据资源
* Web界面
	* 上述所有需求中Web界面开发工作

# 6. 系统设计

## 6.1 元数据

### 6.1.1 URI

元数据是整个资源平台，甚至整个摩羯大数据服务的基础，供资源平台和其他相关平台访问。

我们对所有资源都用URI进行定位，对于不同类型的资源，URI的Schema不同：

* 数据资源: data://[DataType]/[DataUri]
	* DataType: hdfs|rsync|mysql
* 程序资源: script://[ScriptId]
* 任务资源: task://[TaskId]

### 6.1.2 API

平台提供一套标准的接口用于访问读、写的元数据:

	// 用于查询符合条件的资源
	query_resource(const ResourceQueryRequest& request, ResourceQueryResponse* response);
	
	// 用于增、删、改资源
	update_resource(vector<ResProto> insert_resources, vector<ResProto> update_resources, vector<int64_t> delete_resource_ids)

API的参数用Protobuf描述:
	
	// 资源类型
	enum ResType {
		RES_TYPE_DATA_SOURCE = 0;
		RES_TYPE_DATA = 1;
		RES_TYPE_SCRIPT = 2;
		RES_TYPE_TASK = 3;
	}
	
	// 数据源类型
	enum ResDataSourceType {
		DATA_TYPE_HDFS = 0;
		DATA_TYPE_RSYNC = 1;
	}
	
	// 描述数据源资源
	message ResDataSource {
		required ResDataSourceType data_type;
		optional ResDataSourceHDFS data_hdfs;
		optional ResDataSourceRSync data_rsync;
		required string schema;
		repeated string partition;
		optional string etl_script;
	}
	
	// 数据类型
	enum ResDataType {
		DATA_TYPE_HDFS = 0;
		DATA_TYPE_MJSTATS = 1;
	}
	
	// 描述数据资源
	message ResData {
		required ResDataType data_type;
		optional ResDataHDFS data_hdfs;
		optional ResDataMJStats data_mjstats;
		required string schema;
		repeated string partition;
		optional string etl_script;
	}
	
	// 程序资源类型
	enum ResScriptType {
		SCRIPT_TYPE_PYTHON = 0;
		SCRIPT_TYPE_HQL = 1;
	}
	
	// 描述程序资源
	message ResScript {
		required ResScriptType script_type;
		required string script;
		optional byte value;
		optional string value_type;
	}
	
	// 描述任务资源
	message ResTask {
		repeated int res_data_id;
		required int res_script_id;
		required string start_time;
		required int run_interval;
		optional byte value;
		optional string value_type;
	}
	
	// 描述资源
	message ResProto {
		optional int64 id;
		required string name;
		required string chn_name;
		required id group_id;
		required string description;
		required string owner;
		required ResourceType res_type;
		optional string uri;
		optional ResData res_data;
		optional ResScript res_script;
		optional ResTask res_task;
	}
	
	// 数据资源查询请求
	message ResourceQueryRequest {
		required int limit [default=-1];		// 查询数目
		optional string res_name = 1;
		optional string res_chn_name = 2;
		optional int group_id;
		optional string owner;
	}

元数据模块需要提供ResData、ResScript和ResTask与对应URI的转换功能。

### 6.1.3 数据存储

一期，用MySQL的InnoDB表来存储不同资源的元数据，每种资源（数据、程序、任务）的元数据对应MySQL数据库表格中的一行记录，包含如下属性：

* AllResources - 描述所有资源:

| 字段 | 字段类型 | 含义 | 备注 | 
| :--: | :--: | :--- | :--- |
| ResId | bigint | 资源ID | 自增Primary Key |
| ResType | int | 资源类型 | |
| ResName | varchar(128) | 资源名称 (英文) | |
| ResChnName | varchar(128) | 资源名称 (中文) | |
| ResGroupId | bigint | 所属资源组 | 用资源组管理资源，方便权限控制，一期可留空 |
| ResDescription | varchar | 资源详细描述 | |
| ResSchema | varchar | 资源Schema，为一个proto文件 | |
| ResPartition | varchar | Partition Key，逗号分割 | 例如 'Date,UserId' |
| ResURI | varchar | URI，用于定位资源 | |
| ResOwner | char(32) | 资源创始人 | |
| ResCreationTime | datetime | 资源创建时间 | |
| ResModifier | char(32) | 资源最后修改人 | |
| ResModificationTime | datetime | 资源最后修改时间 | |

## 6.2 数据资源管理

如设计思路中所述，这个模块，主要负责根据URI读取多个存储模型下异构的数据，及管理所有数据源的数据导入。

该模块主要以Lib的形式为其他模块提供服务，一期即HDFS、单机和MySQL(MJStats, InnoDB)的数据读写服务。

### 6.2.1 访问HDFS

存储在HDFS上的数据资源有两种形式：Protobuf和文本文件，前者在平台中广泛使用，用于存储大部分原始数据和中间数据，后者主要用于需要导入Hive的数据，一般导入完成后可删除。HDFS上资源的Uri的模式:

	data://hdfs/(SchemaId)/(pb|txt)/(PathOfData)
	
Lib提供读写接口，供其他模块使用:

	* `load_hdfs(string uri, string local_path)`
	* `dump_hdfs(string uri, string local_path)`

### 6.2.2 访问Hive

Hive有自己的数据管理服务，对于需要使用Hive查询的中间数据和结果数据，需要将文件放到特定路径下，并在Hive中执行导入操作。

// TODO

### 6.2.3 访问单机数据

对于存储在单机的数据，要求建立SSH信任关系。资源平台可以提供所有执行任务的“执行机”的SSH密钥。

存储在单机上的数据资源，Uri的模式为:
	
	data://rsync/host/(PathOfData)
	
Lib只提供读接口:

	* `load_rsync(string uri, string local_path)`

### 6.2.4 访问MySQL

一期，资源平台开发特定的程序，将计算后产生的数据资源导入到MySQL(InnoDB/MJStats)中，未来再开放接口，允许用户自定义数据导入到MySQL中。

MySQL数据的查询服务一期只能通过MySQL Client完成。

### 6.2.5 数据源管理

各个异构数据源的例行导入，可以认为是平台的例行任务——通过数据资源管理模块创建的，由特定导入程序定时执行的任务。

例如，用户在资源平台上配置了一个例行的日志抓取任务，每小时从 host-testserver 中抓取 /home/work/log/xxx/%Y%m%d/%H/ 下按年月日小时分隔的数据。则其配置任务时，Web界面上填写数据源为单机，机器名为 host-testserver，路径为 /home/work/log/xxx/%Y%m%d/%H/，设定抓取起始时间，时间间隔为1小时。资源平台会自动发起抓取任务，可能的Uri如：

	data://rsync/host_testserver/home/work/log/xxx/20140101/00/
	data://rsync/host_testserver/home/work/log/xxx/20140101/01/
	......
	
任务会由数据抓取程序完成，并通过用户配置的格式化程序将数据格式化(protobuf)。

## 6.3 程序资源管理

一期，资源平台只支持MJFlows框架下的流式计算，二期会支持Hadoop框架。

MJFlows是任务管理框架，虽然其性能支持准流式计算，但它没有进程调度机制，需要用户自己管理进程（与Hadoop的本质区别）。具体形式后文介绍。

### 6.3.1 MJFlows Worker脚本

一期，除了HQL外，允许用户以编写MJFlows worker的形式，使用流式计算框架。资源平台提供一个Python框架，用户编写Python函数，函数输入都是一行日志，输出也是一行日志。在Python脚本里，平台封装好处理输入输出的过程，提供若干辅助开发的公共函数，提供用于数据监控的机制。

| 序号 | 角色 | 步骤 | 说明 |
| :--: | :--: | :--- | :--- |
| 1 | 用户RD | 登录程序管理平台，编写、测试Python函数 | |
| 2 | 资源平台 | 根据程序配置及用户开发的自定义函数，生成完整的Python脚本供用户下载 | |
| 3 | 用户RD | 用户下载平台生成的Python脚本 | |
| 4 | 用户RD | 登录任务管理平台，新建例行任务或临时任务，获得任务ID | |
| 5 | 资源平台 | 任务管理系统为任务在MJFlows中分配Task group，并在元数据管理系统中添加记录 | |
| 6 | 资源平台 | 任务管理系统定时往Task group中提交任务 | |
| 7 | 用户RD | 用户通过任务ID，启动程序 | |
| 8 | 资源平台 | 用户启动的程序，根据任务ID，从元数据管理系统中读取任务和程序配置 | |
| 9 | 资源平台 | 用户启动的程序，自动从Task group中获取任务，完成任务，输出结果 | |

在二期中，除了Hadoop框架外，MJFlows框架还会加入Partition的支持。支持原始日志按Partition分Worker处理。

### 6.3.2 Python公共函数库

资源平台提供Python函数库，辅助用户开发ETL程序，提供的函数如下:

| 函数 | 说明 |
| :--- | :--- |
| // TODO | |

### 6.3.3 HQL

在资源平台内部，所有HQL程序都会自动生成Python脚本，在MJFlows中执行。

## 6.4 任务资源管理

任务资源管理，主要负责任务的调度和执行。

流式处理平台中，数据流通常分为拉(Pull)和推(Push)两种模式，在资源平台针对这两种模式需要开发两种调度方式：

	* 令牌调度器: 按指定时间间隔例行执行任务，实现Pull模式，例如日志的定时拉取
	* 依赖资源调度器: 监控其依赖的资源，满足条件后发起任务，实现Push模式，例如报表生成，需要依赖前置的ETL任务

对于资源平台中所有任务，在MJFlows中都有一个输入任务组(InputTaskGroup)和输出任务组(OutputTaskGroup)的概念，每个任务的执行程序，从输入任务组中获取任务，完成后将任务从输入任务组中删除，同时往输出任务组中添加任务。

以抓取日志为例，它需要令牌调度器，每小时调度一次。它向MJFlows中的Task group `download_log_in`获取Task。令牌调度器每小时往`download_log_in`插入一个Task。程序获得Task后，抓取日志，将该Task从Task group中删除，并向`download_log_out`中插入一个任务。

以ETL程序为例，如果某个ETL程序每天执行一次，输入为前一天的所有小时级日志。它需要依赖资源调度器，监控其依赖的前一天的24个小时级日志，`download_log_out`中，存在这24个小时级日志的Task时，调度器才能调度ETL任务，向其InputTaskGroup插入Task。

### 6.4.1 令牌调度

令牌调度器可以看作是一个定时向MJFlows插入Task的程序，它保证插入Task的完整性和唯一性（不缺，不多）。

我们通过在元数据管理系统中保存定时任务调度器的调度历史，保证向MJFlows插入Task的完整性和唯一性。MySQL中，记录调度历史的表结构如下：

| 字段 | 字段类型 | 含义 | 备注 | 
| :--: | :--: | :--- | :--- |
| Id | bigint | 自增主键ID | |
| RoutineTaskId | bigint | 任务ID，指向被调度的任务 | |
| RoutineTaskTime | datetime | 任务时间，标示任务调度时间 | 与RoutineTaskId组成Unique Key |
| MJFlowsTaskId | 任务在MJFlows中的ID | |
| CreateTime | timestamp | 记录生成时间 | |

令牌调度器整个插入流程设计如下：

	TicketScheduler启动，清理上一次Scheduler Crash（如果发生了）的现场，并提交新的Task
	0. start transaction
    1. select * from SchedulerState for update; 锁表保证Scheduler唯一性
    2. 提交新的Task
        2.a. 获取需要提交的任务列表
            select * from RoutineTaskList into routine_task_list
            foreach task in routine_task_list
                根据TaskName，在 `RoutineTaskHistory` 表中获取最新的TaskTime
                    若TaskName无记录
                        则将task.start_time作为新任务的时间提交任务
                    若TaskName记录存在
                        若TaskName对应记录数超过设定的执行次数，则不再调度
                        否则判断TaskTime + task.run_interval是否小于当前时间，若小于，则用
                        它作为新任务的时间提交
        2.b 将待提交的任务统一提交
            向 `RoutineTaskHistory` 表批量插入新数据，`TaskId`字段留空
            update MJFLOWS，批量插入Task，并获得TaskId
            将TaskId填入 `RoutineTaskHistory` 表中
    3. commit transaction
    4. 更新MJFlows中已Committed的Task，清理未Committed的Task
        从MJFlows query当前所有task并存在mjflows_task_list中
        foreach task in mjflows_task_list
            if task.state = TO_BE_COMMITTED
                根据TaskName和TaskTime，在 `RoutineTaskHistory` 表中获取对应记录。
                    若记录存在
                        update MJFlows，将task.state设为COMMITTED
                    若记录不存在
                        则将Task删除

### 6.4.2 依赖资源调度

依赖资源任务的调度器，可以认为是MJFlows上的一个Worker，它监控MJFlows上若干TaskGroup，若某个TaskGroup中Task数量和内容满足一定要求后，向后继TaskGroup提交一个新的任务，并删除原有TaskGroup中的任务。

由于MJFlows的Task修改接口能保证Task的增、删的原子性，因此依赖任务调度器的实现较为简单。

### 6.4.3 任务执行

在一期资源平台中，任务分为内部任务和外部任务：

	* 内部任务：完成内部功能，或内部预定义程序的任务，如数据抓取，数据导入Hive/MySQL，报表生成
	* 外部任务：用户自定义脚本程序生成的任务，如自定义ETL

一期只支持MJFlows流式计算框架，不提供进程管理、调度功能，因此需要开发者、用户手动管理所有执行任务的进程。内部任务在系统上线时已经定义好进程的执行机，外部任务需要用户利用自己的机器（或者资源平台提供若干机器）执行。

一期，需要开发的内部任务包括:

	* 单机数据抓取 - 从单机定时抓取数据
	* Hive数据导入
	* MJStats数据导入
	* InnoDB数据导入
	* 标准ETL计算程序 - 对于符合BI SDK的Schema的数据，提供预定义的ETL计算，生成统计结果导入MJStats
	* // TODO

# 7. 备忘录

* 数据存储： 所有在平台的数据都保存2份，Protobuf数据及Txt数据，后者供Hive作为外部表使用
* 程序多Partition输出：程序应该支持多Partition的输出


